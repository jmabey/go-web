package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func Handler() http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		// Does something...
	})
}

func ExampleLog() {
	http.Handle("/", web.Log(Handler()))

	ctx := context.Background()
	http.Handle("/ctx", web.Log(web.Adapter(ctx, FuncHandler())))
}
