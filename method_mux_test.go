package web

import (
	"net/http"
	"reflect"
	"testing"
)

func TestMethodMux(t *testing.T) {
	h := &StubContextHandler{}
	mm := MethodMux{
		http.MethodGet:   h,
		http.MethodPost:  h,
		http.MethodPatch: h,
	}

	if got, want := mm.Methods(), []string{http.MethodGet, http.MethodPatch, http.MethodPost}; !reflect.DeepEqual(got, want) {
		t.Errorf("got slice %q; want %q", got, want)
	}
	if got, want := mm.String(), "GET, PATCH, POST"; got != want {
		t.Errorf("got string %q; want %q", got, want)
	}
}

func TestMethodMuxHandler(t *testing.T) {
	ctx, rw, req := SetUpRequestVars()
	req.Method = http.MethodPost
	hs := []*StubContextHandler{{}, {}}

	MethodMux{
		http.MethodGet:  hs[0],
		http.MethodPost: hs[1],
	}.ServeHTTP(ctx, rw, req)

	if hs[0].Called {
		t.Errorf("unexpected GET handler call")
	}
	if !hs[1].Called {
		t.Errorf("POST handler not called")
	}
	if rw.Status != 0 {
		t.Errorf("unexpected status %d", rw.Status)
	}
	if len(rw.Body) != 0 {
		t.Errorf("unexpected body %q", rw.Body)
	}
}

func TestMethodMuxBadMethod(t *testing.T) {
	ctx, rw, req := SetUpRequestVars()
	h := &StubContextHandler{}

	MethodMux{
		http.MethodGet:  h,
		http.MethodPost: h,
	}.ServeHTTP(ctx, rw, req)

	if h.Called {
		t.Errorf("unexpected call")
	}
	if got, want := rw.Status, http.StatusMethodNotAllowed; got != want {
		t.Errorf("got status %d; want %d", got, want)
	}
	if got, want := string(rw.Body), "Method Not Allowed\n"; got != want {
		t.Errorf("got body %q; want %q", got, want)
	}
}
