package web

import (
	"net/http"
	"sort"
	"strings"

	"golang.org/x/net/context"
)

// MethodMux is a middleware context handler that calls handlers based on the
// request's method.
type MethodMux map[string]ContextHandler

// Methods returns a slice listing all the HTTP methods defined in mm.
func (mm MethodMux) Methods() []string {
	ms := []string{}
	for m := range mm {
		ms = append(ms, m)
	}
	sort.Strings(ms)
	return ms
}

// String returns a comma-separated list of methods in mm.
func (mm MethodMux) String() string {
	return strings.Join(mm.Methods(), ", ")
}

// ServeHTTP calls the appropriate handler based on the request's method. If the
// request's method is not found in mm, then an error response is written.
func (mm MethodMux) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	if h, ok := mm[req.Method]; ok {
		h.ServeHTTP(ctx, rw, req)
		return
	}

	s := http.StatusMethodNotAllowed
	http.Error(rw, http.StatusText(s), s)
}
