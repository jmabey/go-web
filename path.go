package web

import (
	"net/http"
	"strings"
)

// Path splits a request's path on "/" and returns the i'th value or "" if i is
// out of bounds. The leading slash is ignored ("/0/1/2/...").
func Path(req *http.Request, i int) string {
	ps := strings.Split(strings.TrimPrefix(req.URL.Path, "/"), "/")
	if i < 0 || i >= len(ps) {
		return ""
	}
	return ps[i]
}
