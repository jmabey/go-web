package web_test

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/jmabey/go-web"
)

func ExamplePath() {
	req, err := http.NewRequest(http.MethodGet, "/users/123", nil)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(web.Path(req, 1))
	// Output: 123
}
