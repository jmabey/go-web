package web

import (
	"net/http"
	"testing"
)

func TestLog(t *testing.T) {
	rw := &mockWriter{}
	req := &http.Request{}
	h := &StubHandler{}

	Log(h).ServeHTTP(rw, req)

	if !h.Called {
		t.Error("handler not called")
	}
}
