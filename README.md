web
===

[![GoDoc](https://godoc.org/gitlab.com/jmabey/go-web?status.svg)](https://godoc.org/gitlab.com/jmabey/go-web) [![build status](https://gitlab.com/jmabey/go-web/badges/master/build.svg)](https://gitlab.com/jmabey/go-web/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/jmabey/go-web)](https://goreportcard.com/report/gitlab.com/jmabey/go-web)

Context handlers
----------------

* `ContextHandler`: Extends `http.Handler` with a `context.Context` parameter so
  that request-scoped values can be passed amongst handlers.

* `ContextHandlerFunc`: Similar to `http.HandlerFunc`.

* `Adapter`: Enables `ContextHandler`s to be used by acting as an adapter
  between `http.Handler` and `ContextHandler`.

Middleware
----------

* `Log`: Basic logging of requests and responses.

* `CORS`: Handles CORS (cross-origin resource sharing) requests.

* `PatternMux`: Calls handlers by matching regular expressions on the request
  path.

* `MethodMux`: Calls handlers based on the request method.

Utilities
---------

* `Path`: Splits the request path and returns a string from the resulting slice.
