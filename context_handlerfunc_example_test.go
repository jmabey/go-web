package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func FuncHandler( /* Handler configuration... */ ) web.ContextHandler {
	return web.ContextHandlerFunc(func(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
		// Does something...
	})
}

func ExampleContextHandlerFunc() {
	ctx := context.Background()
	http.Handle("/", web.Adapter(ctx, FuncHandler()))
}
