package web

import (
	"net/http"
	"net/url"
	"testing"
)

func TestPath(t *testing.T) {
	for i, tt := range []struct {
		i   int
		out string
	}{
		{-1, ""},
		{0, "zero"},
		{1, "one"},
		{2, "two"},
		{3, ""},
	} {
		req := &http.Request{URL: &url.URL{Path: "/zero/one/two"}}

		if out := Path(req, tt.i); out != tt.out {
			t.Errorf("%d: got %s; got %s", i, out, tt.out)
		}
	}
}
