package web

import (
	"fmt"
	"log"
	"net/http"
)

// LogResponseWriter is a ResponseWriter that records the status code that was
// written.
type LogResponseWriter struct {
	http.ResponseWriter
	Status int
}

// NewLogResponseWriter creates a new LogResponseWriter that writes to rw.
func NewLogResponseWriter(rw http.ResponseWriter) *LogResponseWriter {
	return &LogResponseWriter{rw, http.StatusOK}
}

// WriteHeader stores status s and writes it to the response.
func (l *LogResponseWriter) WriteHeader(s int) {
	l.Status = s
	l.ResponseWriter.WriteHeader(s)
}

// Log is a middleware handler that logs information about the current request
// and its corresponding response.
//
// Example log output:
//
//     request: GET /path 1.2.3.4:5678
//     response: GET /path 1.2.3.4:5678, 200 OK
func Log(h http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		s := fmt.Sprintf("%s %s %s", req.Method, req.URL, req.RemoteAddr)
		log.Printf("request: %s", s)

		lrw := NewLogResponseWriter(rw)
		h.ServeHTTP(lrw, req)

		log.Printf("response: %s, %d %s", s, lrw.Status, http.StatusText(lrw.Status))
	})
}
