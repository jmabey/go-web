package web

import (
	"net/http"
	"testing"
)

const exampleOrigin = "https://example.com"

func TestOriginContains(t *testing.T) {
	ex := Origins{exampleOrigin}

	for i, tt := range []struct {
		os Origins
		o  string
		ok bool
	}{
		{ex, exampleOrigin, true},
		{ex, "http://example.com", false},
		{ex, "https://other.example.com", false},
		{Origins{"*"}, exampleOrigin, true},
	} {
		if got := tt.os.Contains(tt.o); got != tt.ok {
			t.Errorf("%d: got %t; want %t", i, got, tt.ok)
		}
	}
}

func setUpCORS() (*mockWriter, *http.Request, *StubHandler) {
	rw, req := newMockWriter(), &http.Request{Header: http.Header{}}
	req.Header.Set("Origin", exampleOrigin)
	return rw, req, &StubHandler{}
}

func TestCORS(t *testing.T) {
	rw, req, h := setUpCORS()

	CORS(Origins{"*"}, nil, nil, h).ServeHTTP(rw, req)

	if !h.Called {
		t.Error("handler not called")
	}
	if got := rw.Header().Get(HeaderAllowOrigin); got != exampleOrigin {
		t.Errorf("got %s %s; want %s", HeaderAllowOrigin, got, exampleOrigin)
	}
	if got := rw.Header().Get(HeaderVary); got != HeaderOrigin {
		t.Errorf("got %s %s; want %s", HeaderVary, got, HeaderOrigin)
	}
	if len(rw.Body) != 0 {
		t.Errorf("unexpected body %s", rw.Body)
	}
}

func TestCORSPreflight(t *testing.T) {
	rw, req, h := setUpCORS()
	req.Method = http.MethodOptions
	ms := []string{http.MethodGet, http.MethodPost}
	hs := []string{"Authorization", "Content-Type"}

	CORS(Origins{"*"}, ms, hs, h).ServeHTTP(rw, req)

	if h.Called {
		t.Error("unexpected handler call")
	}
	if got, want := rw.Header().Get(HeaderAllowMethods), "GET, POST"; got != want {
		t.Errorf("got %s %q; want %q", HeaderAllowMethods, got, want)
	}
	if got, want := rw.Header().Get(HeaderAllowHeaders), "Authorization, Content-Type"; got != want {
		t.Errorf("got %s %q; want %q", HeaderAllowHeaders, got, want)
	}
	if len(rw.Body) != 0 {
		t.Errorf("unexpected body %q", rw.Body)
	}
}

func TestCORSBadOrigin(t *testing.T) {
	rw, req, h := setUpCORS()
	os := Origins{"https://test.example.com"}

	CORS(os, nil, nil, h).ServeHTTP(rw, req)

	if h.Called {
		t.Error("unexpected handler call")
	}
	if got := rw.Header().Get(HeaderAllowOrigin); got != "" {
		t.Errorf("got %s %q; want empty", HeaderAllowOrigin, got)
	}
	if got := rw.Header().Get(HeaderVary); got != "" {
		t.Errorf("got %s %q; want empty", HeaderVary, got)
	}
	if got, want := rw.Status, http.StatusForbidden; got != want {
		t.Errorf("got status %d; want %d", got, want)
	}
}
