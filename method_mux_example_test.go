package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func ExampleMethodMux() {
	ctx := context.Background()
	http.Handle("/user", web.Adapter(ctx, web.MethodMux{
		http.MethodGet:    FuncHandler(), // Handler for fetching users...
		http.MethodPost:   FuncHandler(), // Handler for creating users...
		http.MethodPatch:  FuncHandler(), // Handler for updating users...
		http.MethodDelete: FuncHandler(), // Handler for deleting users...
	}))
}
