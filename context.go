package web

import (
	"net/http"

	"golang.org/x/net/context"
)

// ContextHandler is a handler that also accepts a context as an additional
// argument.
type ContextHandler interface {
	ServeHTTP(context.Context, http.ResponseWriter, *http.Request)
}

// ContextHandlerFunc is a function that is a ContextHandler.
type ContextHandlerFunc func(context.Context, http.ResponseWriter, *http.Request)

// ServeHTTP calls h(ctx, rw, req).
func (h ContextHandlerFunc) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	h(ctx, rw, req)
}

// Adapter is a middleware handler acts as an adapter between http.Handler and
// ContextHandler. h is called with ctx.
func Adapter(ctx context.Context, h ContextHandler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		h.ServeHTTP(ctx, rw, req)
	})
}
