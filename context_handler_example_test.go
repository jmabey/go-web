package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

type StructHandler struct {
	// Fields can be used for handler configuration...
}

func (h StructHandler) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	// Does something...
}

func ExampleContextHandler() {
	ctx := context.Background()
	http.Handle("/", web.Adapter(ctx, StructHandler{}))
}
