package web

import (
	"net/http"
	"regexp"

	"golang.org/x/net/context"
)

// Pattern associates a regular expression to a context handler.
type Pattern struct {
	Regexp  *regexp.Regexp
	Handler ContextHandler
}

// PatternMux is a middleware context handler that calls handlers based on
// matching the request's path against regular expressions.
type PatternMux []Pattern

// ServeHTTP calls the first handler in pm that matches the request's path.
func (pm PatternMux) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	for _, p := range pm {
		if p.Regexp.MatchString(req.URL.Path) {
			p.Handler.ServeHTTP(ctx, rw, req)
			return
		}
	}

	s := http.StatusNotFound
	http.Error(rw, http.StatusText(s), s)
}
