package web_test

import (
	"net/http"
	"regexp"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func ExamplePatternMux() {
	ctx := context.Background()
	http.Handle("/users/", web.Adapter(ctx, web.PatternMux{
		{
			Regexp:  regexp.MustCompile(`^/users/\d+$`),
			Handler: FuncHandler(), // Handler for fetching a user by ID...
		},
		{
			Regexp:  regexp.MustCompile(`^/users/\d+/comments$`),
			Handler: FuncHandler(), // Handler for fetching a user's comments...
		},
	}))
}
