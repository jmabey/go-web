package web

import (
	"log"
	"net/http"
	"strings"
)

// Origins is a slice of allowed origins. Possible values are domains with
// protocol such as "https://example.com" or "*" to allow all origins.
type Origins []string

// Contains returns true if origin o matches any origin in this slice.
func (os Origins) Contains(o string) bool {
	for _, v := range os {
		if v == "*" || v == o {
			return true
		}
	}
	return false
}

// Names of headers used for CORS.
const (
	HeaderAllowHeaders = "Access-Control-Allow-Headers"
	HeaderAllowMethods = "Access-Control-Allow-Methods"
	HeaderAllowOrigin  = "Access-Control-Allow-Origin"
	HeaderVary         = "Vary"
	HeaderOrigin       = "Origin"
)

// CORS is a middleware handler that handles cross-origin requests according to
// the CORS standard.
//
// os specifies the origins that are allowed to make cross-origin requests. If
// a request does not have a valid origin, a 403 Forbidden response is returned.
//
// Preflight requests are handled. ms specifies the allowed methods and hs
// specifies the allowed headers included in the response to the preflight
// request.
//
// Otherwise, h is called for valid, non-preflight cross-origin requests or
// non-cross-origin requests.
func CORS(os Origins, ms, hs []string, h http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		if o := req.Header.Get(HeaderOrigin); o != "" {
			if !os.Contains(o) {
				log.Println("cors: request Origin not allowed:", o)
				rw.WriteHeader(http.StatusForbidden)
				if _, err := rw.Write(nil); err != nil {
					log.Println("cors: write error:", err)
				}
				return
			}

			head := rw.Header()
			head.Set(HeaderAllowOrigin, o)
			head.Set(HeaderVary, HeaderOrigin)

			if req.Method == http.MethodOptions {
				head.Set(HeaderAllowMethods, strings.Join(ms, ", "))
				head.Set(HeaderAllowHeaders, strings.Join(hs, ", "))
				return
			}
		}

		h.ServeHTTP(rw, req)
	})
}
