package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func ExampleCORS() {
	// POST requests from all origins. Allow Content-Type header in requests.
	http.Handle("/one", web.CORS(web.Origins{"*"}, []string{http.MethodPost}, []string{"Content-Type"}, Handler()))

	// POST requests from https://example.com only. No additional request headers.
	http.Handle("/two", web.CORS(web.Origins{"https://example.com"}, []string{http.MethodPost}, nil, Handler()))
}

func ExampleCORS_methodMux() {
	ctx := context.Background()
	mm := web.MethodMux{http.MethodPost: FuncHandler()}
	http.Handle("/", web.CORS(web.Origins{"https://example.com"}, mm.Methods(), nil, web.Adapter(ctx, mm)))
}

// If you have several handlers to register that all need CORS support
// configured a particular way, write a function.
func ExampleCORS_reuseConfig() {
	ctx := context.Background()
	mw := func(mm web.MethodMux) http.Handler {
		return web.CORS(web.Origins{"*"}, mm.Methods(), nil, web.Adapter(ctx, mm))
	}

	http.Handle("/user", mw(web.MethodMux{http.MethodPost: FuncHandler()}))
	http.Handle("/post", mw(web.MethodMux{ /* Post methods and handlers... */ }))
	http.Handle("/comment", mw(web.MethodMux{ /* Comment methods and handlers... */ }))
}
