package web

import (
	"net/http"
	"net/url"
	"regexp"
	"testing"
)

func TestPatternMux(t *testing.T) {
	ctx, rw, req := SetUpRequestVars()
	req.URL = &url.URL{Path: "/two"}
	hs := []*StubContextHandler{{}, {}}

	PatternMux{
		{
			Regexp:  regexp.MustCompile(`^/one$`),
			Handler: hs[0],
		},
		{
			Regexp:  regexp.MustCompile(`^/two$`),
			Handler: hs[1],
		},
	}.ServeHTTP(ctx, rw, req)

	if hs[0].Called {
		t.Errorf("unexpected handler 0 call")
	}
	if !hs[1].Called {
		t.Errorf("handler 1 not called")
	}
	if rw.Status != 0 {
		t.Errorf("unexpected status %d", rw.Status)
	}
	if len(rw.Body) != 0 {
		t.Errorf("unexpected body %q", rw.Body)
	}
}

func TestPatternMuxNotFound(t *testing.T) {
	ctx, rw, req := SetUpRequestVars()
	req.URL = &url.URL{}

	PatternMux{}.ServeHTTP(ctx, rw, req)

	if got, want := rw.Status, http.StatusNotFound; got != want {
		t.Errorf("got status %d; want %d", got, want)
	}
	if got, want := string(rw.Body), "Not Found\n"; got != want {
		t.Errorf("got body %q; want %q", got, want)
	}
}
