package web

import (
	"net/http"
	"testing"

	"golang.org/x/net/context"
)

func TestAdapter(t *testing.T) {
	ctx, rw, req := SetUpRequestVars()
	call := false

	h := func(hctx context.Context, rw http.ResponseWriter, req *http.Request) {
		if hctx != ctx {
			t.Error("got different context")
		}

		call = true
	}

	Adapter(ctx, ContextHandlerFunc(h)).ServeHTTP(rw, req)

	if !call {
		t.Error("handler not called")
	}
}
