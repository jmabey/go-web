package web_test

import (
	"net/http"

	"gitlab.com/jmabey/go-web"
	"golang.org/x/net/context"
)

func ExampleAdapter() {
	ctx := context.Background()
	http.Handle("/struct", web.Adapter(ctx, StructHandler{}))
	http.Handle("/func", web.Adapter(ctx, FuncHandler()))
}
