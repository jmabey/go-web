package web

import (
	"net/http"

	"golang.org/x/net/context"
)

type mockWriter struct {
	h      http.Header
	Body   []byte
	Status int
}

func newMockWriter() *mockWriter {
	return &mockWriter{h: http.Header{}}
}

func (m mockWriter) Header() http.Header {
	return m.h
}

func (m *mockWriter) Write(b []byte) (int, error) {
	m.Body = b
	return 0, nil
}

func (m *mockWriter) WriteHeader(s int) {
	m.Status = s
}

type StubHandler struct {
	Called bool
}

func (h *StubHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	h.Called = true
}

type StubContextHandler StubHandler

func (h *StubContextHandler) ServeHTTP(ctx context.Context, rw http.ResponseWriter, req *http.Request) {
	h.Called = true
}

func SetUpRequestVars() (context.Context, *mockWriter, *http.Request) {
	return context.Background(), newMockWriter(), &http.Request{}
}
